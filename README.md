# Autodeploy ServerPilot app

## Requirements

### cli tools

- jq (like sed for json)
- bw (bitwarden cli)

### php

extensions:
- ssh2

enabled functions:
- shell_exec()

settings:
- max_execution_time = 120

### apache

if using fcgid or fpm, Timeout and ProxyTimeout need to be set to at least 240 seconds

## config.php

Everything should be self explanatory for the most part, but I'll document each of the constants eventually. The one that was difficult to figure out was the ssh fingerprint (SSH_SERVER_FP). Here are the instructions:

- Fill out at least the SSH_HOST and SSH_PORT
- Run the following code and you'll get the fingerprint

```
<?php
require('./config.php');

$connection = ssh2_connect(SSH_HOST, SSH_PORT);

echo ssh2_fingerprint($connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX) . "\n";
```

## SSH key

You will need a passphraseless SSH key. This should be generated on the server running the script. Use the following:

```
ssh-keygen -f automation -N '' -t rsa -b 4096 -C "automation"
```

Next, copy the contents of `~/.ssh/automation.pub` into `~/.ssh/authorized_keys` of any user on the remote/local server you will be installing apps onto. Don't overwrite, just append!

## Cron

You'll need to add a cron to run the `check_autossl.php` script. This is what checks for AutoSSL and enables it if available. For example:

```
* * * * * /usr/bin/php /path/to/this/file/check_autossl.php
```
## Usage

This script accepts a few parameters which you can see in the example below. You can test it via CLI with the following:

```
curl https://YOURDOMAIN/new_site -d '{"domain":"NEW_DOMAIN", "tagline":"The Best Example Corp In The World", "company_name":"The Example Corp", "gf_contact":"user@email.com"}' -H 'Content-Type: application/json'
```

`gf_contact` is specific to GravityForms which was part of the requirements for the original usage. Likely will be updating the script to accept an array of values to search-replace.
