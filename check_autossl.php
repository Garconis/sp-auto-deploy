<?php

require('config.php');
require('./classes/SpApi.php');

$sp = new SpApi(SPCLIENTID, SPAPI);

$log_file = './autossl_to_check';
$log = fopen($log_file, 'r') or die("Unable to open: " . $log_file);
$content = [];

while (($line = fgets($log)) !== false) {
  $line = str_replace(array("\n","\r"), '', $line);
  if (!(empty($line))) {
    preg_match('/^\s*/', $line, $matches);
    if (empty($matches[1])) {
      array_push($content, $line);
    } else { echo "matches checking: " . $line . "\n" . $matches[1] . "\n"; }
  } else { echo "empty line? " . $line . "\n"; }
}

fclose($log);

foreach ($content as $key => $app_id) {
    echo "getting app data for " . $app_id . "\n";
    $result = $sp->getApp($app_id);
    if ($result == 1) {
        continue;
    }
    $app_info = $result["data"];
    $ssl_info = $app_info["autossl"];

    if ($ssl_info["available"] == true) {
        echo "enabling ssl\n";
        $enable_ssl = $sp->enableSSL($app_id);
        echo "enabling redirect\n";
        $enable_redirect = $sp->enableRedirect($app_id);

        unset($content[$key]);
    }
}

$log = fopen($log_file, 'w') or die("Unable to open: " . $log_file);

foreach ($content as $key => $appId) {
  fwrite($log, $appId . "\n");
}

?>