<?php

require('functions.php');
require('config.php');
// ServerPilot API class
require('classes/SpApi.php');
// SSH class
require('classes/ssh.php');
// Bitwarden cli
require('classes/bw-cli.php');

$app_name;
$log_entry;
$skip_empty = array();

$sp = new SpApi(SPCLIENTID, SPAPI);

$received_data = json_decode(file_get_contents("php://input"), true);
$domain = $received_data['domain'];
$tagline = $received_data['tagline'];
$company_name = $received_data['company_name'];
$gf_contact = $received_data['gf_contact'];

// Split up the domain so we can create an appname and devdomain
if (preg_match("/(([^.]+)\.){1,}([^.]+)/", $domain)) {
    logDebug('Valid domain provided: ' . $domain);
} else {
    logError('Invalid domain provided: ' . $domain);
    die();
}

if (preg_match("/^.+\.com$/", $domain)) {
	$domain = preg_replace('/\.com$/', '', $domain);
}

$app_name = preg_replace('/\./', '-', $domain);
$dev_domain = $app_name . ".freshy.site";

// Create new app and throw app id into a file so AutoSSL things can be done afterward
$create_result = $sp->createApp($domain, $app_name, $dev_domain);
if ($create_result[0] === false) {
    logError('Unable to create app: ' . $create_result[1]);
    die();
}
$log_new_app = fopen('./autossl_to_check', "a");
fwrite($log_new_app, $create_result["data"]["id"] . "\n");
fclose($log_new_app);

$log_entry = "Created application" . "\n" . json_encode($create_result['data']);
logInfo($log_entry);

$max_time = 15;
for ($time=1; $time<=$max_time; ++$time) {
	if ($max_time%$time == 0) {
		logDebug("Application still configuring on server...");
	}
	sleep(1);
}

$path = '/srv/users/' . SPUSER . '/apps/' . $app_name . '/public/';
$wp_cli = '/usr/bin/wp --path=' . $path;

$install_ai1wm = $wp_cli . ' plugin install all-in-one-wp-migration ' . UAI1WM;
$activate_plugins = $wp_cli . ' plugin activate --all 2>&1';

$dl_template = 'wget ' . TEMPLATEURL . ' -O ' . $path . 'wp-content/ai1wm-backups/latest.wpress 2>&1';
$restore_template = 'echo y | ' . $wp_cli . ' ai1wm restore latest.wpress 2>&1';

$perma_links = $wp_cli . ' option update permalink_structure "$(' . $wp_cli . ' option get permalink_structure)" 2>&1';

$rm_plugins = $wp_cli . ' plugin delete akismet hello 2>&1';

$rm_themes = $wp_cli . ' theme delete twentyseventeen twentynineteen twentytwenty 2>&1';

$update_core = $wp_cli . ' core update 2>&1';
$update_plugins = $wp_cli . ' plugin update --all 2>&1';
$update_themes = $wp_cli . ' theme update --all 2>&1';

$mailgun_get = $wp_cli . ' option get mailgun --format=json';
$mailgun_update = $wp_cli . ' option update mailgun ';
$mailgun_info = '';

$footer_get = $wp_cli . ' option get et_divi --format=json';
$footer_update = $wp_cli . ' option update et_divi ';
$footer_info = '';

$db_update = $wp_cli . ' core update-db';

$domain_search_replace = $wp_cli . ' search-replace \'http://' . $dev_domain . '\' \'https://' . $dev_domain . '\' --all-tables 2>&1';

if (!empty($gf_contact)) {
    $gf_update = $wp_cli . ' search-replace \'{admin_email}\' \'' . $gf_contact . '\' --all-tables';
} else {
    $skip_empty[] = 'gf_contact';
}
if (!empty($tagline)) {
    $tagline_update = $wp_cli . ' option update blogdescription "' . $tagline . '"';
} else {
    $skip_empty[] = 'tagline_update';
}
if (!empty($company_name)) {
    $site_title_update = $wp_cli . ' option update blogname \'' . $company_name . '\'';
} else {
    $skip_empty[] = 'site_title_update';
}

$cmdList = array(
    "install_ai1wm" => $install_ai1wm,
    "activate_plugins" => $activate_plugins,
    "dl_template" => $dl_template,
    "restore_template" => $restore_template,
    "perma_links" => $perma_links,
    "perma_links2" => $perma_links,
    "rm_plugins" => $rm_plugins,
    "rm_themes" => $rm_themes,
    "update_core" => $update_core,
    "db_update" => $db_update,
    "update_themes" => $update_themes,
    "tagline_update" => $tagline_update,
    "mailgun_get" => $mailgun_get,
    "mailgun_update" => $mailgun_update,
    "gf_update" => $gf_update,
    "footer_get" => $footer_get,
    "footer_update" => $footer_update,
    "site_title_update" => $site_title_update,
    "update_plugins" => $update_plugins,
    "domain_search_replace" => $domain_search_replace,
    );

$ssh = new NiceSSH();
$ssh->connect();

$log_entry = $app_name . " customizations have started";
logInfo($log_entry);

// SCP a default WordPress htaccess
$ssh->scp('./wp-htaccess', $path . '.htaccess');

foreach ($cmdList as $key => $value) {
    if (in_array($key, $skip_empty)) {
        $log_entry = $app_name . "::" . $key . "::skipping";
        logInfo($log_entry);
        continue;
    }
    if ($key == "mailgun_update") {
        $mailgun_info->{'from-address'} = $app_name . '@sender.freshysites.com';
        $mailgun_info->{'from-name'} = $company_name;
        $value = $value . '\'' . json_encode($mailgun_info) . '\' --format=json';
    }
    if ($key == "footer_update") {
        $footer_credits = str_replace("Site Name Here", $company_name, $footer_info->{'custom_footer_credits'});
        $footer_info->{'custom_footer_credits'} = $footer_credits;
        $value = $value . '\'' . json_encode($footer_info) . '\' --format=json';
    }
    $log_entry = $app_name . "::" . $key . "::start\n" . $value;
    logInfo($log_entry);
    $result = $ssh->exec($value);
    if ($key == "mailgun_get") {
        $mailgun_info = json_decode($result);
    }
    if ($key == "footer_get") {
        $footer_info = json_decode($result);
    }
    $log_entry = $result . "\n" . $app_name . "::" . $key . "::end";
    logInfo($log_entry);
}

$wp_users = ['freshysites' => BWTEAMID, 'DOps' => BWDOPSID];

foreach ($wp_users as $wp_user => $colId) {
    $log_entry = $app_name . '::password reset::' . $wp_user . "::start";
    logInfo($log_entry);
    
    $rand_pass = randomPassword();
    
    $cmd = $wp_cli . ' user update ' . $wp_user . ' --user_pass=\'' . $rand_pass . '\';';
    
    $result = $ssh->exec($cmd);
    $bwResult = bwcreds($dev_domain, $rand_pass, $wp_user, $colId);
    if ($bwResult === true) {
        $log_entry = $result . "\n" . $app_name . '::password reset::' . $wp_user . "::end";
        logInfo($log_entry);
    }
}

$log_entry = $app_name . " is now customized";
logInfo($log_entry);

$ssh->disconnect();

