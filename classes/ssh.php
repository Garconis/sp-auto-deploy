<?php

class NiceSSH {

    // SSH Host
    public $ssh_host;
    // SSH Port
    public $ssh_port;
    // SSH Server Fingerprint
    public $ssh_server_fp;
    // SSH Username
    public $ssh_auth_user;
    // SSH Public Key File
    public $ssh_auth_pub;
    // SSH Private Key File
    public $ssh_auth_priv;
    // SSH Private Key Passphrase (null == no passphrase)
    public $ssh_auth_pass;
    public $connection;


    function __construct() {
        $this->ssh_host = SSH_HOST;
        $this->ssh_port = SSH_PORT;
        $this->ssh_server_fp = SSH_SERVER_FP;
        $this->ssh_auth_user = SSH_AUTH_USER;
        $this->ssh_auth_pub = SSH_AUTH_PUB;
        $this->ssh_auth_priv = SSH_AUTH_PRIV;
        $this->ssh_auth_pass;
    }
    
    public function connect() {
        if (!($this->connection = ssh2_connect($this->ssh_host, $this->ssh_port))) {
            throw new Exception('Cannot connect to server');
        }
        $fingerprint = ssh2_fingerprint($this->connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX);
        if (strcmp($this->ssh_server_fp, $fingerprint) !== 0) {
            throw new Exception('Unable to verify server identity!' . $fingerprint);
        }
        if (!ssh2_auth_pubkey_file($this->connection, $this->ssh_auth_user, $this->ssh_auth_pub, $this->ssh_auth_priv, $this->ssh_auth_pass)) {
            throw new Exception('Authentication rejected by server');
        }
    }
    public function exec($cmd) {
        if (!($stream = ssh2_exec($this->connection, $cmd))) {
            throw new Exception('SSH command failed');
        }
        stream_set_blocking($stream, true);
        $data = "";
        while ($buf = fread($stream, 4096)) {
            $data .= $buf;
        }
        fclose($stream);
        return $data;
    }
    public function scp($local_file, $remote_file) {
        $scp_output = ssh2_scp_send($this->connection, $local_file, $remote_file, 0644);
        if ($scp_output == 1) {
            logDebug("SCP results: Success!");
            return true;
        } else {
            logInfo("SCP results: Failure!");
            return false;
        }
    }
    public function disconnect() {
        $this->exec('echo "EXITING" && exit;');
    }
    public function __destruct() {
        $this->disconnect();
    }
    public function info() {
        return $this->connection;
    }
}
?>