<?php

function bwcreds($domain, $password, $user, $colId) {
    $bw = '/usr/local/bin/bw';
    for ($x=1; $x<=3; $x++) {
        $command = $bw . ' logout';
        $result = shell_exec($command);
        logDebug('Bitwarden logout: ' . $result);

        $command = 'echo "' . BWPASS . '" | ' . $bw . ' login ' . BWUSER . ' --raw 2>/dev/null';

        $BW_SESSION = shell_exec($command);
        if (!empty($BW_SESSION)) {
            logDebug('Retrieved Bitwarden session on attempt #' . $x);
            break;
        } else {
            logDebug('Could not obtain Bitwarden session on attempt #' . $x);
        }
    }
    
    if (empty($BW_SESSION)) {
        logError("Couldn't create Bitwarden session");
        return false;
    }
    
    $command = $bw . ' --session ' . $BW_SESSION . ' get template item | jq \'.name = "' . $domain . '" | .login.password = "' . $password . '" | .login.uris = [{"uri": "https://' . $domain . '/wp-admin"}] | .login.username = "' . $user . '" | .organizationId = "' . BWORGID . '" | .notes = null\' | ' . $bw . ' encode | ' . $bw . ' --session ' . $BW_SESSION . ' create item 2>/dev/null';
    $result = shell_exec($command);
    if (empty($result)) {
        logError("Couldn't create saved creds for $user on $domain");
        return false;
    }
    
    $result = json_decode($result);
    $id = $result->id;

    $command = 'echo \'["' . $colId . '"]\' | ' . $bw . ' encode | ' . $bw . ' --session ' . $BW_SESSION . ' edit item-collections ' . $id . ' 2>/dev/null';
    $result = shell_exec($command);
    if (empty($result)) {
        logError("Couldn't change collection for saved creds for $user on $domain");
        return false;
    }

    $command = $bw . ' logout 2>/dev/null';
    $result = shell_exec($command);
    return true;
}