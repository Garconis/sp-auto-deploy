<?php

class log2f {
    public $logFile;
    public $logObj;
    
    function __construct($logFile) {
        $this->logFile = $logFile;
        $this->logObj = fopen($this->logFile, 'a') or die('Unable to open: ' . $this->logFile);
    }
    
    function info($msg) {
        $this->logObj = fopen($this->logFile, 'a') or die('Unable to open: ' . $this->logFile);
        fwrite($this->logObj, $msg . "\n");
    }
    
    function password($msg) {
        $this->logObj = fopen($this->logFile, 'a') or die('Unable to open: ' . $this->logFile);
        fwrite($this->logObj, $msg);
    }
    
    function __destruct() {
        fclose($this->logObj);
    }
}

?>