<?php
error_reporting(E_ALL & ~E_NOTICE);

class SpApi
{
    public $format;
    public $output;
    public $curl;
    public $data;
    
    function __construct ($SPCLIENTID, $SPAPI) {
        if (!function_exists('curl_init')) {
            die("This requires that curl+ssl support is compiled into the PHP interpreter\n");
        }
        $this->set_format("json");
        $this->spclientid = $SPCLIENTID;
        $this->spapi = $SPAPI;
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_USERAGENT, 'Freshy API 1.0' );
        if (isset($_SERVER["PHP_CURL_SSL_VERIFY_HOSTNAME"]) && !$_SERVER["PHP_CURL_SSL_VERIFY_HOSTNAME"]) {
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
        }
    }
    
    function __destruct () {
        curl_close($this->curl);
    }
    
    public function set_format ($format) {
        $valid_formats = [
            "simplexml" => "xml",
            "xml" => "xml",
            "json" => "json",
            "yaml" => "yaml",
        ];
        if ($valid_formats[$format]) {
            $this->format = $format;
            $this->output = $valid_formats[$format];
            return $this->format;
        }
        else {
            die("set_format requires that the format is xml, json, yaml or simplexml\n");
        }
    }
    
    public function parseHTTP ($response) {
        # parse the response into a header array and content variables
        $pos = 0;
        if (preg_match("/(\S+) (\d+)\s*([^\r\n]*)\r?\n/", $response, $matches, 0, $pos)) {
            $status_code = $matches[2];
            $status_mesg = $matches[3];
            $pos += strlen($matches[0]);
        }
        $headers = [];
        while (1) {
            if (!isset($response[$pos])) {
                break;
            }
            elseif (preg_match("/\G([^:\s]+):\s*([^\r\n]*)\r?\n/", $response, $matches, 0, $pos)) {
                $headers[$matches[1]] = $matches[2];
                $pos += strlen($matches[0]);
            }
            elseif (preg_match("/\G\r?\n/", $response, $matches, 0, $pos)) {
                $pos += strlen($matches[0]);
                break;
            }
            else {
                break;
            }
        }
        $content = substr($response, $pos);
        $href = [
            "status_code" => $status_code,
            "status_mesg" => $status_mesg,
            "headers" => $headers,
            "content" => $content,
        ];
        return $href;
    }
    
    private function setCreds () {
        curl_setopt($this->curl, CURLOPT_USERPWD, $this->spclientid . ':' . $this->spapi);
    }
    
    private function request ($function, $args = []) {
        if (!$function) {
            die("request requires that a function is defined\n");
        }
        
        $api_url = SPHOST . $function;

        if (!empty($args)) {
            $data = json_encode($args);
        }

        curl_setopt($this->curl, CURLOPT_URL, $api_url);
        curl_setopt($this->curl, CURLOPT_HEADER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($this->curl, CURLOPT_USERPWD, $this->spclientid . ':' . $this->spapi);
        $this->setCreds();

        if (isset($data)) {
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($this->curl, CURLOPT_POST, 1);
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
        }
        
        $response = curl_exec($this->curl);

        $href = $this->parseHTTP($response);

        if ($href["status_code"] != 200 && $href["status_code"] != 400) {
            echo "request failed: " . $href["status_code"] . "\n" . $api_url . "\n" . $href["content"] . "\n" . $data . "\n";
            return [false, $href["content"]];
            //die("request failed: $href[status_code] $href[content]->error \n");
        }
        
        unset($args);
        unset($data);
        
        return json_decode($href["content"], true);
    }
    
    // functioning
    public function listServers() {
        return $this->request("servers");
    }
    
    // functioning
    // need to add validation for things such as app_name max-length 30
    public function createApp($domain, $app_name, $dev_domain) {
        // $example_data='{"name": "wordpress", "sysuserid": "RvnwAIfuENyjUVnl", "runtime": "php7.0", "domains": ["example.com", "www.example.com"], "wordpress": {"site_title": "My WordPress Site", "admin_user": "admin", "admin_password": "mypassword", "admin_email": "example@example.com"}}';
        
        $data = new stdClass();
        $wordpress = new stdClass();
        $data->name = $app_name;
        $data->sysuserid = SPUSERID;
        $data->runtime = SPRUNTIME;
        $data->domains = [ $dev_domain, 'www.' . $dev_domain ];
        $wordpress->site_title = $app_name;
        $wordpress->admin_user = 'freshysites';
        $wordpress->admin_password = 'freshysites';
        $wordpress->admin_email = 'wp@freshysites.com';
        $data->wordpress = $wordpress;
        
        return $this->request("apps", $data);
    }
    
    // functioning
    public function listApps() {
        return $this->request("apps");
    }
    
    public function getApp($appId) {
        return $this->request("apps/" . $appId);
    }
    
    // functioning
    public function enableSSL($appId) {
        $data = new stdClass();
        $data->auto = true;
        return $this->request("apps/" . $appId . "/ssl", $data);
    }
    
    public function enableRedirect($appId) {
        $data = new stdClass();
        $data->force = true;
        return $this->request("apps/" . $appId . "/ssl", $data);
    }
    
    // functioning
    public function deleteApp($appId) {
        return $this->request("apps/" . $appId);
    }
    
    public function initialMaintenance() {
        /*
            perform updates; --all
            delete plugins; akistmet, hello
        */
    }
    
    public function configureTemplate() {
        /*
            install ai1wm
                wp plugin install all-in-one-wp-migration
            install unlimited ai1wm
            wget template file via ssh
            restore template
            update permalinks
                wp option get permalink_structure
                wp option update permalink_structure '/%postname%'
        */
    }
    
    public function finalMaintenance() {
        /*
            delete year themes; 17, 19, 20 (fresh WP install from SP) (might even be able to just regex this somehow or maybe wp-cli somehow supports wildcard patterns?)
        */
    }
    
    // stretch goal IMO, especially depending on API limitations
    public function resetUserPass() {
        /*
            reset freshysites user password
            save to bitwarden vault
                dev domain
                change ownership to FreshySites
                add to Team collection
        */
    }

}

?>