<?php

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-_=+{}[],.|';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 24; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

/**
 * Log errors to error and info log as defined in config.php
 *
 * @param string $msg Error message to print.
 * @param bool $trace (Optional) If true, add backtrace to error.
 */
function logError( $msg, $trace = false ) {

    $time = date( 'Y-m-d H:i:s' );
	$str = ' ' . print_r( $msg, true ) . "\n";
	if ( $trace ) {
		$str .= getBacktrace() . "\n";
	}
    logDebug('ERROR: ' . $str);
	file_put_contents( FILE_ERROR_LOG, $time . $str, FILE_APPEND );
}

/**
 * Logs debug info to debug log as defined in config.php
 *
 * @param string $msg Debug message to print
 * @param null $data (Optional) Array to print after output
 */
function logDebug( $msg, $data = null ) {
	$str = date( 'Y-m-d H:i:s' ) . ' ' . print_r( $msg, true ) . "\n";
	if ( $data ) {
		$str .= print_r( $data, true ) . "\n";
	}
	file_put_contents( FILE_DEBUG_LOG, $str, FILE_APPEND );
}

/**
 * Logs informational messages to the info and debug logs as defined in config.php
 *
 * @param string $msg Info message to print
 */
function logInfo( $msg ) {
	$str = date( 'Y-m-d H:i:s' ) . ' ' . print_r( $msg, true ) . "\n";
	file_put_contents( FILE_DEBUG_LOG, $str, FILE_APPEND );
	file_put_contents( FILE_INFO_LOG, $str, FILE_APPEND );
}

function getBacktrace() {
	return getExceptionTraceAsString(new Exception);
}

function getExceptionTraceAsString(Exception $exception) {
    $rtn = '';
    $count = 0;
    foreach ($exception->getTrace() as $frame) {
        $args = '';
        if (isset($frame['args'])) {
            $args = array();
            foreach ($frame['args'] as $arg) {
                if (is_string($arg)) {
                    $args[] = "'" . $arg . "'";
                } elseif (is_array($arg)) {
                    $args[] = 'Array';
                } elseif ($arg === null) {
                    $args[] = 'NULL';
                } elseif (is_bool($arg)) {
                    $args[] = $arg ? 'true' : 'false';
                } elseif (is_object($arg)) {
                    $args[] = get_class($arg);
                } elseif (is_resource($arg)) {
                    $args[] = get_resource_type($arg);
                } else {
                    $args[] = $arg;
                }
            }
            $args = implode(', ', $args);
        }
        $rtn .= sprintf( "#%s %s(%s): %s(%s)\n",
            $count,
            $frame['file'],
            $frame['line'],
            $frame['function'],
            $args );
        $count++;
    }
    return $rtn;
}